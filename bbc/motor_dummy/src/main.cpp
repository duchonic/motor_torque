#include <arduino.h>


uint8_t seconds = 0;
uint8_t minutes = 0;
uint8_t hours = 0;

typedef enum { 
  STATE_INITIAL, 
  STATE_SPEEED_LOW_TORQUE, 
  STATE_PAUSE, 
  STATE_HIGH_TORQUE, 
  NUM_STATES 
} state_t;

uint8_t speed = 0;
uint8_t torque = 0;
uint8_t pause = 0;

state_t state = STATE_INITIAL;

void setup() {
  Serial.begin(115200);
}

void time_inc() {
    seconds += 1;
    if (seconds >= 60) {
      seconds = 0;
      if (minutes < 59) {
        minutes += 1;
      }
      else {
        minutes = 0;
        hours += 1;
      }
    }
    delay(50);
}

void loop() {
  switch(state)
  {
    case STATE_INITIAL: {
      if (pause > 0) {
        pause -= 1;
      }
      else {
        torque = 0;
        speed = 2;
        pause = 80;
        state = STATE_SPEEED_LOW_TORQUE;
        Serial.println("STATE_SPEED_LOW_TORQUE");
      }
      break;
    }
    case STATE_SPEEED_LOW_TORQUE: {


      if (speed > 2) {
        speed = 0;
      }
      else {
        speed += 1;
      }

      // increase speed, torque somewhere between 2010 and 1990
      if (torque > 10) {
        torque = 0;
      }
      else {
        torque += 1;
      }

      if (pause > 0) {
        pause -= 1;
      }
      else {
        torque = 0;
        speed = 0;
        state = STATE_PAUSE;
        pause = 5;
        Serial.println("PAUSE");
      }
      break;
    }
    case STATE_PAUSE: {
      if (pause > 0) {
        pause -= 1;
      }
      else {
        state = STATE_HIGH_TORQUE;
        speed = 0;
        Serial.println("STATE_HIGH_TORQUE");
        torque = 0;
      }
      break;
    }
    case STATE_HIGH_TORQUE: {
      if (torque < 250) {
        torque += 4;
      }
      else {
        torque = 200;
        pause = 5;
        state = STATE_INITIAL;
        Serial.println("STATE_INITIAL");
      }
      break;
    }
    default: {
      Serial.println("unknown state");
    }
   }

    time_inc();
    char stringBuffer80[80];        // fastprint is limited to 90 characters so this must be < 90
    // send speed
    sprintf(stringBuffer80, "%02u:%02u:%02u.428  I  [goswissdri]  Got b'10 0c 00 00 1c'", hours, minutes, seconds);
    Serial.println(stringBuffer80);
    sprintf(stringBuffer80, "%02u:%02u:%02u.430  I  [goswissdri]  Got b'01 0c %02x 07 ff'", hours, minutes, seconds, torque);
    Serial.println(stringBuffer80);

    // send torque
    sprintf(stringBuffer80, "%02u:%02u:%02u.428  I  [goswissdri]  Got b'10 0e 00 00 1c'", hours, minutes, seconds);
    Serial.println(stringBuffer80);
    sprintf(stringBuffer80, "%02u:%02u:%02u.430  I  [goswissdri]  Got b'01 0e %02x 00 ff'", hours, minutes, seconds, speed);
    Serial.println(stringBuffer80);

}