import yaml

class Test():
    """\
    Test Class to collect data. Calculate min/max.
    Busyflag and some configuration for the test.
    """

    min = 0
    max = 0
    values = []
    measure_counts = 0
    actual_counts = 0
    running = False
    fail = False
    speed_max = 0
    speed_min = 0
    average = 0

    def __init__(self) -> None:
        pass

    def Reset(self) -> None:
        self.min = 10000
        self.max = 0
        self.average = 0
        self.actual_counts = 0
        self.values.clear()
        self.fail = False
        self.runnin = False

    def Update(self, value) -> None:
        self.actual_counts += 1
        if value < self.min:
            self.min = value
        if value > self.max:
            self.max = value
        self.values.append(value)
        self.average = int(sum(self.values)/len(self.values))

    def Finished(self) -> bool:
        if self.actual_counts >= self.measure_counts:
            self.running = False
            return True
        else:
            return False

# end of class Test

class TestTorqueRest(Test):
    test_min = 0
    test_max = 0
    test_diff = 0

    def __init__(self) -> None:
        with open('config/config.yaml', 'r') as file:
            self.config = yaml.safe_load(file)
        file.close()
        self.speed_min = self.config['rest']['speed']['min']
        self.speed_max = self.config['rest']['speed']['max']
        self.measure_counts = self.config['rest']['measure_counts'] 
        self.test_min = self.config['rest']['torque']['min']
        self.test_max = self.config['rest']['torque']['max']
        self.test_diff = self.config['rest']['torque']['diff']


class TestTorqueForce(Test):
    torque_threshold = 0
    
    def __init__(self) -> None:
        with open('config/config.yaml', 'r') as file:
            self.config = yaml.safe_load(file)
        file.close()
        self.speed_min = self.config['force']['speed']['min']
        self.speed_max = self.config['force']['speed']['max']
        self.measure_counts = self.config['force']['measure_counts'] 
        self.torque_threshold = self.config['force']['torque_threshold']


class TestTorqueEnd(Test):
    torque_threshold = 0
    
    def __init__(self) -> None:
        with open('config/config.yaml', 'r') as file:
            self.config = yaml.safe_load(file)
        file.close()
        self.measure_counts = self.config['end']['measure_counts'] 

