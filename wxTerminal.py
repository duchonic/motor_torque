#!/usr/bin/env python
#
# A simple application with wxPython for 
# motor cartridge test
#
# (C) 2023 codestation <codestation@bluewin.ch>

import codecs
from serial.tools.miniterm import unichr
import serial
import threading
import wx
import wx.lib.newevent
import wxSerialConfigDialog
import re
from datetime import datetime
import matplotlib.pyplot as plt
import xlsxwriter
import random
import yaml
import os

import test

import config

try:
    unichr
except NameError:
    unichr = chr

# ----------------------------------------------------------------------
# Create an own event type, so that GUI updates can be delegated
# this is required as on some platforms only the main thread can
# access the GUI without crashing. wxMutexGuiEnter/wxMutexGuiLeave
# could be used too, but an event is more elegant.

SerialRxEvent, EVT_SERIALRX = wx.lib.newevent.NewEvent()
SERIALRX = wx.NewEventType()

# ----------------------------------------------------------------------

ID_START_REST = 1
ID_START_FORCE = 2
ID_START_END = 11
ID_SHOW_DATA = 3
ID_CLEAR = 4
ID_SAVEAS = 5
ID_SETTINGS = 6
ID_TERM = 7
ID_EXIT = 8
ID_RTS = 9
ID_DTR = 10
TIMER_ID = 10

NEWLINE_CR = 0
NEWLINE_LF = 1
NEWLINE_CRLF = 2

class TerminalSetup:
    """
    Placeholder for various terminal settings. Used to pass the
    options to the TerminalSettingsDialog.
    """
    def __init__(self):
        self.echo = False
        self.unprintable = False
        self.newline = NEWLINE_CRLF


class TerminalSettingsDialog(wx.Dialog):
    """Simple dialog with common terminal settings like echo, newline mode."""

    def __init__(self, *args, **kwds):
        self.settings = kwds['settings']
        del kwds['settings']
        # begin wxGlade: TerminalSettingsDialog.__init__
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.checkbox_echo = wx.CheckBox(self, -1, "Local Echo")
        self.checkbox_unprintable = wx.CheckBox(self, -1, "Show unprintable characters")
        self.radio_box_newline = wx.RadioBox(self, -1, "Newline Handling", choices=["CR only", "LF only", "CR+LF"], majorDimension=0, style=wx.RA_SPECIFY_ROWS)
        self.sizer_4_staticbox = wx.StaticBox(self, -1, "Input/Output")
        self.button_ok = wx.Button(self, wx.ID_OK, "")
        self.button_cancel = wx.Button(self, wx.ID_CANCEL, "")


        self.__set_properties()
        self.__do_layout()
        # end wxGlade
        self.__attach_events()
        self.checkbox_echo.SetValue(self.settings.echo)
        self.checkbox_unprintable.SetValue(self.settings.unprintable)
        self.radio_box_newline.SetSelection(self.settings.newline)

    def __set_properties(self):
        # begin wxGlade: TerminalSettingsDialog.__set_properties
        self.SetTitle("Terminal Settings")
        self.radio_box_newline.SetSelection(0)
        self.button_ok.SetDefault()
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: TerminalSettingsDialog.__do_layout
        sizer_2 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer_4_staticbox.Lower()
        sizer_4 = wx.StaticBoxSizer(self.sizer_4_staticbox, wx.VERTICAL)
        sizer_4.Add(self.checkbox_echo, 0, wx.ALL, 4)
        sizer_4.Add(self.checkbox_unprintable, 0, wx.ALL, 4)
        sizer_4.Add(self.radio_box_newline, 0, 0, 0)
        sizer_2.Add(sizer_4, 0, wx.EXPAND, 0)
        sizer_3.Add(self.button_ok, 0, 0, 0)
        sizer_3.Add(self.button_cancel, 0, 0, 0)
        sizer_2.Add(sizer_3, 0, wx.ALL | wx.ALIGN_RIGHT, 4)
        self.SetSizer(sizer_2)
        sizer_2.Fit(self)
        self.Layout()
        # end wxGlade

    def __attach_events(self):
        self.Bind(wx.EVT_BUTTON, self.OnOK, id=self.button_ok.GetId())
        self.Bind(wx.EVT_BUTTON, self.OnCancel, id=self.button_cancel.GetId())

    def OnOK(self, events):
        """Update data with new values and close dialog."""
        self.settings.echo = self.checkbox_echo.GetValue()
        self.settings.unprintable = self.checkbox_unprintable.GetValue()
        self.settings.newline = self.radio_box_newline.GetSelection()
        self.EndModal(wx.ID_OK)

    def OnCancel(self, events):
        """Do not update data but close dialog."""
        self.EndModal(wx.ID_CANCEL)

# end of class TerminalSettingsDialog


class TerminalFrame(wx.Frame):
    """Simple terminal program for wxPython"""

    def __init__(self, *args, **kwds):
        self.serial = serial.Serial()
        self.serial.timeout = 0.5   # make sure that the alive event can be checked from time to time
        self.settings = TerminalSetup()  # placeholder for the settings
        self.thread = None
        self.alive = threading.Event()
        # begin wxGlade: TerminalFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)

        self.test_torque_rest = test.TestTorqueRest()
        self.test_torque_force = test.TestTorqueForce()
        self.test_torque_end = test.TestTorqueEnd()

        self.label_serial = wx.StaticText(self, -1, "Serial:")
        self.serial_entry = wx.TextCtrl(self, -1, '1234', style=wx.TE_PROCESS_ENTER)
        self.label_name = wx.StaticText(self, -1, "Name:")
        self.name_entry = wx.TextCtrl(self, -1, 'name', style=wx.TE_PROCESS_ENTER)
        self.button_start_rest = wx.Button(self, wx.ID_ANY, "start rest test")
        self.button_start_force = wx.Button(self, wx.ID_ANY, "start force test")
        self.button_start_end = wx.Button(self, wx.ID_ANY, "start torque end")
        self.button_stop = wx.Button(self, wx.ID_ANY, "stop test")
        self.button_showdata = wx.Button(self, wx.ID_ANY, "show data")

        self.gauge_test_rest = wx.Gauge(self, range=self.test_torque_rest.measure_counts)
        self.gauge_test_force = wx.Gauge(self, range=self.test_torque_force.measure_counts)
        self.gauge_test_end = wx.Gauge(self, range=self.test_torque_end.measure_counts)

        # Menu Bar
        self.frame_terminal_menubar = wx.MenuBar()
        wxglade_tmp_menu = wx.Menu()

        self.rx_line = ""

        self.test_name = ''
        self.workbook = xlsxwriter.Workbook('result.xlsx')
        self.worksheet = self.workbook.add_worksheet()

        self.torque_values_y = []
        self.torque_values_x = []
        self.speed_values_y = []
        self.speed_values_x = []

        self.actual_speed = 0

        self.test_result_ok = False

        wxglade_tmp_menu.Append(ID_START_REST, "&StartRest", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.Append(ID_START_FORCE, "&StartForce", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.Append(ID_SHOW_DATA, "&ShowData", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.Append(ID_CLEAR, "&Clear", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.Append(ID_SAVEAS, "&Save Text As...", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.AppendSeparator()
        wxglade_tmp_menu.Append(ID_TERM, "&Terminal Settings...", "", wx.ITEM_NORMAL)
        wxglade_tmp_menu.AppendSeparator()
        wxglade_tmp_menu.Append(ID_EXIT, "&Exit", "", wx.ITEM_NORMAL)
        self.frame_terminal_menubar.Append(wxglade_tmp_menu, "&File")
        wxglade_tmp_menu = wx.Menu()
        wxglade_tmp_menu.Append(ID_RTS, "RTS", "", wx.ITEM_CHECK)
        wxglade_tmp_menu.Append(ID_DTR, "&DTR", "", wx.ITEM_CHECK)
        wxglade_tmp_menu.Append(ID_SETTINGS, "&Port Settings...", "", wx.ITEM_NORMAL)
        self.frame_terminal_menubar.Append(wxglade_tmp_menu, "Serial Port")
        self.SetMenuBar(self.frame_terminal_menubar)
        # Menu Bar end
        self.text_ctrl_output = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE | wx.TE_READONLY)

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_MENU, self.OnStartRest, id=ID_START_REST)
        self.Bind(wx.EVT_MENU, self.OnStartForce, id=ID_START_FORCE)
        self.Bind(wx.EVT_MENU, self.OnStartEnd, id=ID_START_FORCE)
        self.Bind(wx.EVT_MENU, self.OnShowData, id=ID_SHOW_DATA)
        self.Bind(wx.EVT_MENU, self.OnClear, id=ID_CLEAR)
        self.Bind(wx.EVT_MENU, self.OnSaveAs, id=ID_SAVEAS)
        self.Bind(wx.EVT_MENU, self.OnTermSettings, id=ID_TERM)
        self.Bind(wx.EVT_MENU, self.OnExit, id=ID_EXIT)
        self.Bind(wx.EVT_MENU, self.OnRTS, id=ID_RTS)
        self.Bind(wx.EVT_MENU, self.OnDTR, id=ID_DTR)
        self.Bind(wx.EVT_MENU, self.OnPortSettings, id=ID_SETTINGS)

        # end wxGlade
        self.__attach_events()          # register events
        self.OnPortSettings(None)       # call setup dialog on startup, opens port
        if not self.alive.is_set():
            self.Close()

    def StartThread(self):
        """Start the receiver thread"""
        self.thread = threading.Thread(target=self.ComPortThread)
        self.thread.daemon = True
        self.alive.set()
        self.thread.start()
        self.serial.rts = True
        self.serial.dtr = True
        self.frame_terminal_menubar.Check(ID_RTS, self.serial.rts)
        self.frame_terminal_menubar.Check(ID_DTR, self.serial.dtr)

    def StopThread(self):
        """Stop the receiver thread, wait until it's finished."""
        if self.thread is not None:
            self.alive.clear()          # clear alive event for thread
            self.thread.join()          # wait until thread has finished
            self.thread = None

    def __set_properties(self):
        # begin wxGlade: TerminalFrame.__set_properties
        self.SetTitle("Serial Terminal")
        self.SetSize((546, 383))
        self.text_ctrl_output.SetFont(wx.Font(9, wx.MODERN, wx.NORMAL, wx.NORMAL, 0, ""))
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: TerminalFrame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_1.Add(self.text_ctrl_output, 1, wx.EXPAND, 0)

        sizer_basic = wx.BoxSizer(wx.HORIZONTAL)
        sizer_basic.Add(self.label_serial)
        sizer_basic.Add(self.serial_entry, wx.EXPAND)
        sizer_basic.Add(self.label_name)
        sizer_basic.Add(self.name_entry, wx.EXPAND)
        sizer_1.Add(sizer_basic, 0, wx.EXPAND, 0)
        hbox_1 = wx.BoxSizer(wx.HORIZONTAL)
        hbox_1.Add(self.button_start_rest)
        hbox_1.Add(self.gauge_test_rest, wx.EXPAND)
        sizer_1.Add(hbox_1, 0, wx.EXPAND )
        hbox_2 = wx.BoxSizer(wx.HORIZONTAL)
        hbox_2.Add(self.button_start_force)
        hbox_2.Add(self.gauge_test_force, wx.EXPAND)
        sizer_1.Add(hbox_2, 0, wx.EXPAND)
        hbox_3 = wx.BoxSizer(wx.HORIZONTAL)
        hbox_3.Add(self.button_start_end)
        hbox_3.Add(self.gauge_test_end, wx.EXPAND)
        sizer_1.Add(hbox_3, 0, wx.EXPAND)
        sizer_1.Add(self.button_stop)
        #sizer_1.Add(self.button_showdata)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    def __attach_events(self):

        self.button_start_force.Bind(wx.EVT_BUTTON, self.OnStartForce)
        #self.button_start_rest.Bind(wx.EVT_BUTTON, self.OnStartRest)
        self.button_start_end.Bind(wx.EVT_BUTTON, self.OnStartEnd)
        #self.button_showdata.Bind(wx.EVT_BUTTON, self.OnShowData)
        self.button_stop.Bind(wx.EVT_BUTTON, self.OnStop)

        # register events at the controls
        self.Bind(wx.EVT_MENU, self.OnStartRest, id=ID_START_REST)
        self.Bind(wx.EVT_MENU, self.OnStartForce, id=ID_START_FORCE)
        self.Bind(wx.EVT_MENU, self.OnClear, id=ID_CLEAR)
        self.Bind(wx.EVT_MENU, self.OnSaveAs, id=ID_SAVEAS)
        self.Bind(wx.EVT_MENU, self.OnExit, id=ID_EXIT)
        self.Bind(wx.EVT_MENU, self.OnPortSettings, id=ID_SETTINGS)
        self.Bind(wx.EVT_MENU, self.OnTermSettings, id=ID_TERM)

        with open('config/config.yaml', 'r') as file:
            self.config = yaml.safe_load(file)

        if 'config' in self.config.keys():
            if self.config['config']['simulation']:
                print("create timer for simulation")
               
                self.sim_timer = wx.Timer(self, TIMER_ID) 
                self.sim_timer.Start(10)
                self.Bind(wx.EVT_TIMER, self.OnTimer, id=TIMER_ID)
                print('simulation ON')
            else:
                self.Bind(EVT_SERIALRX, self.OnSerialRead)
        else:
            print('simulation key not found')
            self.Bind(EVT_SERIALRX, self.OnSerialRead)

        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.serial_entry.Bind(wx.EVT_TEXT_ENTER, self.OnEnter)

    def OnTimer(self, event):
        speed = ["00", "01", "02", "03", "04"]
        torque = ["ff", "fe", "fc", "fd", "f9", "f2"]
        actual_speed = random.choice(speed)

        if actual_speed == "00":
            self.WriteText("08:08:08.430  I  [goswissdri]  Got b'01 0e " + actual_speed + " 00 ff'\n")
            #self.WriteText("08:08:08.430  I  [goswissdri]  Got b'01 0c " + random.choice(torque) + " 08 ff'\n")
            self.WriteText("08:08:08.430  I  [goswissdri]  Got b'01 0c " + random.choice(torque) + " 07 ff'\n")
        else:
            self.WriteText("08:08:08.430  I  [goswissdri]  Got b'01 0e " + actual_speed + " 00 ff'\n")
            self.WriteText("08:08:08.430  I  [goswissdri]  Got b'01 0c " + random.choice(torque) + " 07 ff'\n")
            #self.WriteText("08:08:08.430  I  [goswissdri]  Got b'01 0c " + random.choice(torque) + " 17 ff'\n")


    def OnEnter(self, event): 
        self.OnStartRest(1)

    def OnExit(self, event):  # wxGlade: TerminalFrame.<event_handler>
        """Menu point Exit"""
        self.Close()

    def OnClose(self, event):
        """Called on application shutdown."""
        self.StopThread()               # stop reader thread
        self.serial.close()             # cleanup
        self.Destroy()                  # close windows, exit app

    def OnSaveAs(self, event):  # wxGlade: TerminalFrame.<event_handler>
        """Save contents of output window."""
        with wx.FileDialog(
                None,
                "Save Text As...",
                ".",
                "",
                "Text File|*.txt|All Files|*",
                wx.SAVE) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                filename = dlg.GetPath()
                with codecs.open(filename, 'w', encoding='utf-8') as f:
                    text = self.text_ctrl_output.GetValue().encode("utf-8")
                    f.write(text)

    def OnStartRest(self, event):

        file_path = "../data/"
        if not os.path.exists(file_path):
            print('path doesnt exist, create folder')
            os.makedirs(file_path)


        filename = '../data/' + datetime.now().strftime("%Y%m%d_%H%M%S") + '_' +  str(self.serial_entry.GetValue() ) + '_result.xlsx'
        print(filename)
        self.test_name = '../data/' + datetime.now().strftime("%Y%m%d_%H%M%S") + '_' +  str(self.serial_entry.GetValue() )
        self.serial_entry.SetValue('')
        self.workbook = xlsxwriter.Workbook(filename)
        self.worksheet = self.workbook.add_worksheet()

        self.test_result_ok = True

        self.torque_values_y.clear()
        self.torque_values_x.clear() 
        self.speed_values_y.clear() 
        self.speed_values_x.clear() 

        self.OnStop(1)

        self.text_ctrl_output.AppendText("====> start rest test <====\n");
        self.text_ctrl_output.AppendText("speed min/max: " + str(self.test_torque_rest.speed_min) + '/' + str(self.test_torque_rest.speed_max) + '\n')
        print(">>>> on start rest")
        self.test_torque_rest.running = True

        self.test_torque_rest.Reset()

        self.button_start_rest.SetBackgroundColour(wx.YELLOW)
        self.Refresh()


    def OnStartForce(self, event):
        if self.test_torque_force.running:
            self.test_torque_force.running = False
            self.test_result_ok = False



            self.worksheet.write_column(0, 10, ['result'])
            self.worksheet.write_column(1, 10, ['Force FAIL'])

            self.OnSaveData()
            self.OnCloseData()


            self.button_start_force.SetBackgroundColour( wx.RED )
            self.Refresh()
        else:
            self.text_ctrl_output.AppendText("====> start force test <====\n");
            self.text_ctrl_output.AppendText("speed max: " + str(self.test_torque_force.speed_max) + '\n')
            self.text_ctrl_output.AppendText("torque min: " + str(self.test_torque_rest.average + self.test_torque_force.torque_threshold) + '\n')
            print(">>>> on start force")
            self.test_torque_force.running = True

            self.test_torque_force.Reset()

            self.button_start_force.SetBackgroundColour( wx.YELLOW )
            self.Refresh()




    def OnStartEnd(self, event):
        if self.test_torque_end.running:
            self.test_torque_end.running = False
            self.test_result_ok = False

            self.worksheet.write_column(0, 10, ['result'])
            self.worksheet.write_column(1, 10, ['End FAIL'])

            self.OnSaveData()
            self.OnCloseData()

            self.button_start_end.SetBackgroundColour(wx.RED)
            self.Refresh()
        else:
            self.text_ctrl_output.AppendText("====> start end test <====\n");
            self.text_ctrl_output.AppendText("torque max: " + str(self.test_torque_rest.max) + '\n')
            print(">>>> on start end")
            self.test_torque_end.running = True

            self.test_torque_end.Reset()

            self.button_start_end.SetBackgroundColour(wx.YELLOW)
            self.Refresh()



    def OnStop(self, event):
        self.test_torque_rest.running = False
        self.test_torque_force.running = False
        self.test_torque_end.running = False

        self.text_ctrl_output.AppendText("stop test\n");

        self.gauge_test_rest.SetValue(0)
        self.gauge_test_force.SetValue(0)
        self.gauge_test_end.SetValue(0)

        self.button_start_rest.SetBackgroundColour(wx.WHITE)
        self.button_start_force.SetBackgroundColour(wx.WHITE)
        self.button_start_end.SetBackgroundColour(wx.WHITE)
        
    def OnClear(self, event):  # wxGlade: TerminalFrame.<event_handler>
        """Clear contents of output window."""
        self.text_ctrl_output.Clear()

    def OnPortSettings(self, event):  # wxGlade: TerminalFrame.<event_handler>
        """
        Show the port settings dialog. The reader thread is stopped for the
        settings change.
        """
        if event is not None:           # will be none when called on startup
            self.StopThread()
            self.serial.close()
        ok = False
        while not ok:
            with wxSerialConfigDialog.SerialConfigDialog(
                    self,
                    -1,
                    "",
                    show=wxSerialConfigDialog.SHOW_BAUDRATE | wxSerialConfigDialog.SHOW_FORMAT | wxSerialConfigDialog.SHOW_FLOW,
                    serial=self.serial) as dialog_serial_cfg:
                dialog_serial_cfg.CenterOnParent()
                result = dialog_serial_cfg.ShowModal()
            # open port if not called on startup, open it on startup and OK too
            if result == wx.ID_OK or event is not None:
                try:
                    self.serial.open()
                except serial.SerialException as e:
                    with wx.MessageDialog(self, str(e), "Cartridge tester ", wx.OK | wx.ICON_ERROR)as dlg:
                        dlg.ShowModal()
                else:
                    self.StartThread()
                    self.SetTitle("Cartridge tester V1.1 on {} [{}]".format(
                        self.serial.portstr,
                        self.serial.baudrate,
                        ))
                    ok = True
            else:
                # on startup, dialog aborted
                self.alive.clear()
                ok = True

    def OnTermSettings(self, event):  # wxGlade: TerminalFrame.<event_handler>
        """\
        Menu point Terminal Settings. Show the settings dialog
        with the current terminal settings.
        """
        with TerminalSettingsDialog(self, -1, "", settings=self.settings) as dialog:
            dialog.CenterOnParent()
            dialog.ShowModal()


    def WriteText(self, text):
        self.rx_line += text
        solve = self.rx_line.split('\n') 

        for entries in solve[:-1]:

            if "b'01 0c" in entries:
                try:
                    current_dateTime = datetime.now()
                    self.torque_values_x.append(current_dateTime.second * 1000000 + current_dateTime.minute * 60 * 1000000 + current_dateTime.microsecond)
                except:
                    print("parsing time error")

                try:
                    data = entries.strip().split()[6:8][::-1]
                    s = bytes.fromhex(''.join(data) )   
                    value = int.from_bytes(s, 'big', signed=True)
                    # log values for graph
                    self.torque_values_y.append(value)

                    speed_min = self.test_torque_rest.speed_min
                    speed_max = self.test_torque_rest.speed_max
                    if self.test_torque_rest.running and self.actual_speed >= speed_min and self.actual_speed <= speed_max:
                        self.text_ctrl_output.AppendText(".")
                        print("torque: ", value)

                        self.test_torque_rest.Update(value)
                        self.gauge_test_rest.SetValue(self.test_torque_rest.actual_counts)
                        if self.test_torque_rest.Finished():
                            
                            self.text_ctrl_output.AppendText("\ntorque rest min/max/diff: " + str(self.test_torque_rest.min) + " / " + str(self.test_torque_rest.max) + " / " + str(abs(self.test_torque_rest.max - self.test_torque_rest.min)) + '\n') 

                            if self.test_torque_rest.min < self.test_torque_rest.test_min:
                                self.test_result_ok = False
                                self.test_torque_rest.fail = True
                                self.text_ctrl_output.AppendText("min to low: " + str(self.test_torque_rest.min) + " (" + str(self.test_torque_rest.test_min) + ")\n") 
                            
                            if self.test_torque_rest.max > self.test_torque_rest.test_max:
                                self.test_result_ok = False
                                self.test_torque_rest.fail = True
                                self.text_ctrl_output.AppendText("max to high: " + str(self.test_torque_rest.max) + " (" + str(self.test_torque_rest.test_max) + ")\n") 
                            
                            if abs(self.test_torque_rest.max - self.test_torque_rest.min) > self.test_torque_rest.test_diff:
                                self.test_result_ok = False
                                self.test_torque_rest.fail = True
                                self.text_ctrl_output.AppendText("diff to high: " + str(abs(self.test_torque_rest.max - self.test_torque_rest.min)) + " (" + str(self.test_torque_rest.test_diff) + ")\n") 

                            if self.test_torque_rest.fail:
                                self.test_result_ok = False
                                self.button_start_rest.SetBackgroundColour(wx.RED)
                                self.Refresh()
                                self.text_ctrl_output.AppendText("====> torque rest FAILED <===== \n\n") 
                            else:
                                self.button_start_rest.SetBackgroundColour(wx.GREEN)
                                self.Refresh()
                                print("<<<< torque rest ok\n")


                            self.worksheet.write_column(0, 0, ['rest min'])
                            self.worksheet.write_column(1, 0, [self.test_torque_rest.min])
                            self.worksheet.write_column(0, 1, ['rest max'])
                            self.worksheet.write_column(1, 1, [self.test_torque_rest.max])
                            self.worksheet.write_column(0, 2, ['rest average'])
                            self.worksheet.write_column(1, 2, [self.test_torque_rest.average])
                            self.worksheet.write_column(0, 3, ['data min/max'])
                            self.worksheet.write_column(1, 3, self.test_torque_rest.values)

                            self.worksheet.write_column(0, 12, ['rest diff'])
                            self.worksheet.write_column(1, 12, [abs(self.test_torque_rest.max - self.test_torque_rest.min)])
                            self.worksheet.write_column(0, 13, ['rest diff ref'])
                            self.worksheet.write_column(1, 13, [self.test_torque_rest.test_diff])
                            self.worksheet.write_column(0, 14, ['rest min ref'])
                            self.worksheet.write_column(1, 14, [self.test_torque_rest.test_min])
                            self.worksheet.write_column(0, 15, ['rest max ref'])
                            self.worksheet.write_column(1, 15, [self.test_torque_rest.test_max])


                            self.text_ctrl_output.AppendText("====> torque rest finished <===== \n\n") 

                            if self.test_result_ok:
                                self.OnStartForce(1)
                            else:
                                self.worksheet.write_column(0, 10, ['result'])
                                self.worksheet.write_column(1, 10, ['FAIL'])

                                self.OnSaveData()
                                self.OnCloseData()


                    speed_max = self.test_torque_force.speed_max
                    speed_min = self.test_torque_force.speed_min
                    if self.test_torque_force.running and self.actual_speed >= speed_min and self.actual_speed <= speed_max:
                        if value > (self.test_torque_rest.average + self.test_torque_force.torque_threshold):
                            self.text_ctrl_output.AppendText(".")
                            print("torque: ", value)

                            self.test_torque_force.Update(value) 
                            self.gauge_test_force.SetValue(self.test_torque_force.actual_counts)

                            if self.test_torque_force.Finished():
                                self.text_ctrl_output.AppendText("\ntorque force min/max: " + str(self.test_torque_force.min) + " / " + str(self.test_torque_force.max) + '\n') 
                                self.text_ctrl_output.AppendText("====> torque force finished <===== \n\n") 
                            
                                self.button_start_force.SetBackgroundColour(wx.GREEN)
                                self.Refresh()

                                print("<<<< torque force ok\n")

                                self.worksheet.write_column(0, 4, ['force max'])
                                self.worksheet.write_column(1, 4, [self.test_torque_force.max])
                                self.worksheet.write_column(0, 5, ['data force'])
                                self.worksheet.write_column(1, 5, self.test_torque_force.values)
                                
                                self.OnSaveData()
                                self.OnStartEnd(1)

                    #if self.torque_end_running:
                    if self.test_torque_end.running:
                        if value < self.test_torque_rest.max:
                            self.text_ctrl_output.AppendText(".")
                            print("torque: ", value)
                            
                            self.test_torque_end.Update(value)
                            self.gauge_test_end.SetValue(self.test_torque_end.actual_counts)

                            if self.test_torque_end.Finished():

                                self.text_ctrl_output.AppendText("\ntorque end min/max: " + str(self.test_torque_end.min) + " / " + str(self.test_torque_end.max) + '\n') 
                                self.text_ctrl_output.AppendText("====> torque end finished <===== \n\n") 
                            
                                self.button_start_end.SetBackgroundColour(wx.GREEN)
                                self.Refresh()

                                print("<<<< torque end ok\n")

                                self.worksheet.write_column(0, 6, ['end min'])
                                self.worksheet.write_column(1, 6, [self.test_torque_end.min])
                                self.worksheet.write_column(0, 7, ['end max'])
                                self.worksheet.write_column(1, 7, [self.test_torque_end.max])
                                self.worksheet.write_column(0, 8, ['data end'])
                                self.worksheet.write_column(1, 8, self.test_torque_end.values)

                                self.OnSaveData()
                                self.OnCloseData()


                except:
                    print('parsing data error')
                
            if "b'01 0e" in entries:
                try:
                    current_dateTime = datetime.now()
                    self.speed_values_x.append(current_dateTime.second * 1000000 + current_dateTime.minute * 60 * 1000000 + current_dateTime.microsecond)
                except:
                    print("parsing time error")

                try:
                    data = entries.strip().split()[6:8][::-1]
                    s = bytes.fromhex(''.join(data) )   
                    value = int.from_bytes(s, 'big', signed=True)
                    self.speed_values_y.append(value)
                    self.actual_speed = value                
                except:
                    print('parsing data error')
        
        self.rx_line = solve[-1]

    def OnSaveData(self):
        fig, ax = plt.subplots(3, 1, layout="constrained")
        ax[0].plot(self.torque_values_x, self.torque_values_y)  # Plot some data on the axes.
        if self.test_torque_rest.max == self.test_torque_rest.min:
            ax[0].set_ylim([self.test_torque_rest.min, self.test_torque_rest.max+1])
        else:
            ax[0].set_ylim([self.test_torque_rest.min, self.test_torque_rest.max])

        ax[1].plot(self.torque_values_x, self.torque_values_y)  # Plot some data on the axes.
        ax[2].plot(self.speed_values_x, self.speed_values_y)  # Plot some data on the axes.

        fig.savefig(self.test_name + '.png')

    def OnCloseData(self):
        self.worksheet.write_column(0, 9, ['tester'])
        name = [self.name_entry.GetValue()]
        self.worksheet.write_column(1, 9, name)

        self.workbook.close()


    def OnShowData(self, event):

        fig, ax = plt.subplots(3, 1, layout="constrained")
        ax[0].plot(self.torque_values_x, self.torque_values_y)  # Plot some data on the axes.
        
        if self.test_torque_rest.max == self.test_torque_rest.min:
            ax[0].set_ylim([self.test_torque_rest.min, self.test_torque_rest.max+1])
        else:
            ax[0].set_ylim([self.test_torque_rest.min, self.test_torque_rest.max])

        ax[1].plot(self.torque_values_x, self.torque_values_y)  # Plot some data on the axes.
        ax[2].plot(self.speed_values_x, self.speed_values_y)  # Plot some data on the axes.

        fig.show()


    def OnSerialRead(self, event):
        """Handle input from the serial port."""
        self.WriteText(event.data.decode('UTF-8', 'replace'))

    def ComPortThread(self):
        """\
        Thread that handles the incoming traffic. Does the basic input
        transformation (newlines) and generates an SerialRxEvent
        """
        while self.alive.is_set():
            b = self.serial.read(self.serial.in_waiting or 1)
            if b:
                # newline transformation
                if self.settings.newline == NEWLINE_CR:
                    b = b.replace(b'\r', b'\n')
                elif self.settings.newline == NEWLINE_LF:
                    pass
                elif self.settings.newline == NEWLINE_CRLF:
                    b = b.replace(b'\r\n', b'\n')
                wx.PostEvent(self, SerialRxEvent(data=b))

    def OnRTS(self, event):  # wxGlade: TerminalFrame.<event_handler>
        self.serial.rts = event.IsChecked()

    def OnDTR(self, event):  # wxGlade: TerminalFrame.<event_handler>
        self.serial.dtr = event.IsChecked()

# end of class TerminalFrame


class MyApp(wx.App):
    def OnInit(self):
        frame_terminal = TerminalFrame(None, -1, "")
        self.SetTopWindow(frame_terminal)
        frame_terminal.Show(True)
        return 1

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()