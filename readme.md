# cartridge tester YouMo

## version

### V1.0

* three tests (rest, force and end)
* name of tester in excelfile
* serial and timestamp in filename

#### open issues

* main *.xlsx file with all the stuff inside

### V1.1

* upgrade every cartridge gets a serial
* logging also for failed cartridge


## config.yaml

The config.yaml file is used to configure the different tests.
There are three main tests. 

* rest
* force
* end

Every test has separate block with all the configurations: 

```yaml
# the rest test turns the cartridge slowly
# without any torque
rest:
  speed:
    min: 1 
    max: 3
  measure_counts: 10
  torque:
    min: 1950
    max: 2050
    diff: 40
```
## build release with pyinstaller 

Use --add-data to add the config.yaml file to the release folder

mac os:
> pyinstaller wxTerminal.py --add-data="config/config.yaml:config/"

windows:
> pyinstaller wxTerminal.py --add-data="config/config.yaml;config/"
## Buglist Version 1.0

### too many plots (2023/09/14)

Oliver had a problem within the console log that matplotlib has more than 20 plots opened. 
Somehow the software loops throu the documentation state which generates the plots. 
This should only called once.

No need to restart the sniffer, also the *.png file for the corresponding cartridge was documented in the data folder. Just a warning on the console.

### serial number on every cartridge plus min/max/diff in documentation

Every cartridge should get a serial number.

The documentation should write down the cause of the failure in the documentation file.

