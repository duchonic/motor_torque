import test
import yaml

class TestTest():
    test = test.Test()

    def test_minimum(self):
        self.test.Reset()
        self.test.Update(10)
        assert self.test.min == 10
        assert self.test.max == 10
        assert self.test.average == 10
        self.test.Update(20)
        assert self.test.min == 10
        assert self.test.max == 20
        assert self.test.average == 15, "average is not correct"
 
# TODO min/max/average should be tested in TestTest class
class TestTorqueRest():
    torque = test.TestTorqueRest()
    with open('config/config.yaml', 'r') as file:
        config = yaml.safe_load(file)
    def test_minimum(self):
        self.torque.Reset()
        self.torque.Update(300)
        assert self.torque.min == 300
        self.torque.Update(100)
        assert self.torque.min == 100
        self.torque.Update(200)
        assert self.torque.min == 100
    def test_maximum(self):
        self.torque.Reset()
        self.torque.Update(200)
        assert self.torque.max == 200
        self.torque.Update(400)
        assert self.torque.max == 400
        self.torque.Update(300)
        assert self.torque.max == 400
        assert self.torque.average == 300

    def test_speed(self):
        assert self.torque.speed_min == self.config['rest']['speed']['min'] 
        assert self.torque.speed_max == self.config['rest']['speed']['max']


# TODO min/max/average should be tested in TestTest class
class TestTorqueForce():
    torque = test.TestTorqueForce()
    with open('config/config.yaml', 'r') as file:
        config = yaml.safe_load(file)
    def test_minimum(self):
        self.torque.Reset()
        self.torque.Update(300)
        assert self.torque.min == 300
        self.torque.Update(100)
        assert self.torque.min == 100
        self.torque.Update(200)
        assert self.torque.min == 100
    def test_maximum(self):
        self.torque.Reset()
        self.torque.Update(200)
        assert self.torque.max == 200
        assert len(self.torque.values) == 1
        assert self.torque.average == 200
        self.torque.Update(400)
        assert self.torque.max == 400
        assert len(self.torque.values) == 2
        self.torque.Update(300)
        assert self.torque.max == 400
        assert self.torque.average == 300

    def test_speed(self):
        assert self.torque.speed_min == self.config['force']['speed']['min'] 
        assert self.torque.speed_max == self.config['force']['speed']['max']
        assert self.torque.torque_threshold == self.config['force']['torque_threshold']
    
    def test_finish(self):
        self.torque.Reset()
        assert self.torque.Finished() == False
        for _ in range(20):
            self.torque.Update(1)
        assert self.torque.Finished() == True


# TODO finish should be tested in TestTest
class TestTorqueEnd():
    torque = test.TestTorqueEnd()

    def test_finish(self):
        self.torque.Reset()
        for i in range(20):
            self.torque.Update(100)
        assert self.torque.Finished() == True